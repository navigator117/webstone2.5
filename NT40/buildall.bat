cd genrand
nmake /f genrand.mak CFG="genrand - Win32 Debug"
nmake /f genrand.mak CFG="genrand - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd webclient
nmake /f webclient.mak CFG="webclient - Win32 Debug"
nmake /f webclient.mak CFG="webclient - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd webmaster
nmake /f webmaster.mak CFG="webmaster - Win32 Debug"
nmake /f webmaster.mak CFG="webmaster - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd ws20_cgi
nmake /f ws20_cgi.mak CFG="ws20_cgi - Win32 Debug"
nmake /f ws20_cgi.mak CFG="ws20_cgi - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd ws20_iis
nmake /f ws20_iis.mak CFG="ws20_iis - Win32 Debug"
nmake /f ws20_iis.mak CFG="ws20_iis - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd ws20_ns
nmake /f ws20_ns.mak CFG="ws20_ns - Win32 Debug"
nmake /f ws20_ns.mak CFG="ws20_ns - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd ws25_cgi
nmake /f ws25_cgi.mak CFG="ws25_cgi - Win32 Debug"
nmake /f ws25_cgi.mak CFG="ws25_cgi - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd ws25_iis
nmake /f ws25_iis.mak CFG="ws25_iis - Win32 Debug"
nmake /f ws25_iis.mak CFG="ws25_iis - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..

cd ws25_ns
nmake /f ws25_ns.mak CFG="ws25_ns - Win32 Debug"
nmake /f ws25_ns.mak CFG="ws25_ns - Win32 Release"
del /q *.plg Release\*.obj Release\*.ilk Release\*.pch Release\vc50.*
del /q *.plg Debug\*.obj Debug\*.ilk Debug\*.pch Debug\vc50.*
cd ..
