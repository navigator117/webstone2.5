# Microsoft Developer Studio Generated NMAKE File, Based on webmaster.dsp
!IF "$(CFG)" == ""
CFG=webmaster - Win32 Debug
!MESSAGE No configuration specified. Defaulting to webmaster - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "webmaster - Win32 Release" && "$(CFG)" !=\
 "webmaster - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "webmaster.mak" CFG="webmaster - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "webmaster - Win32 Release" (based on\
 "Win32 (x86) Console Application")
!MESSAGE "webmaster - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "webmaster - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\webmaster.exe"

!ELSE 

ALL : "$(OUTDIR)\webmaster.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\bench.obj"
	-@erase "$(INTDIR)\errexit.obj"
	-@erase "$(INTDIR)\getopt.obj"
	-@erase "$(INTDIR)\gettimeofday.obj"
	-@erase "$(INTDIR)\parse_file_list.obj"
	-@erase "$(INTDIR)\rexec.obj"
	-@erase "$(INTDIR)\statistics.obj"
	-@erase "$(INTDIR)\sysdep.obj"
	-@erase "$(INTDIR)\timefunc.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\webmaster.obj"
	-@erase "$(OUTDIR)\webmaster.exe"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /ML /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D\
 "_MBCS" /D "_MT" /Fp"$(INTDIR)\webmaster.pch" /YX /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\webmaster.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=wsock32.lib kernel32.lib user32.lib gdi32.lib winspool.lib\
 comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib\
 odbc32.lib odbccp32.lib libcmt.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)\webmaster.pdb" /machine:I386 /nodefaultlib:"libc.lib"\
 /out:"$(OUTDIR)\webmaster.exe" 
LINK32_OBJS= \
	"$(INTDIR)\bench.obj" \
	"$(INTDIR)\errexit.obj" \
	"$(INTDIR)\getopt.obj" \
	"$(INTDIR)\gettimeofday.obj" \
	"$(INTDIR)\parse_file_list.obj" \
	"$(INTDIR)\rexec.obj" \
	"$(INTDIR)\statistics.obj" \
	"$(INTDIR)\sysdep.obj" \
	"$(INTDIR)\timefunc.obj" \
	"$(INTDIR)\webmaster.obj"

"$(OUTDIR)\webmaster.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE=$(InputPath)
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\webmaster.exe"
   copy release\webmaster.exe ..\..\bin
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "webmaster - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\webmaster.exe"

!ELSE 

ALL : "$(OUTDIR)\webmaster.exe"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\bench.obj"
	-@erase "$(INTDIR)\errexit.obj"
	-@erase "$(INTDIR)\getopt.obj"
	-@erase "$(INTDIR)\gettimeofday.obj"
	-@erase "$(INTDIR)\parse_file_list.obj"
	-@erase "$(INTDIR)\rexec.obj"
	-@erase "$(INTDIR)\statistics.obj"
	-@erase "$(INTDIR)\sysdep.obj"
	-@erase "$(INTDIR)\timefunc.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\webmaster.obj"
	-@erase "$(OUTDIR)\webmaster.exe"
	-@erase "$(OUTDIR)\webmaster.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MLd /W3 /Gm /GX /Zi /Od /D "_DEBUG" /D "_MT" /D "WIN32" /D\
 "_CONSOLE" /D "_MBCS" /Fp"$(INTDIR)\webmaster.pch" /YX /Fo"$(INTDIR)\\"\
 /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\webmaster.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=libcmtd.lib wsock32.lib kernel32.lib user32.lib gdi32.lib\
 winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib\
 uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /incremental:no\
 /pdb:"$(OUTDIR)\webmaster.pdb" /debug /machine:I386 /nodefaultlib:"libcd.lib"\
 /out:"$(OUTDIR)\webmaster.exe" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\bench.obj" \
	"$(INTDIR)\errexit.obj" \
	"$(INTDIR)\getopt.obj" \
	"$(INTDIR)\gettimeofday.obj" \
	"$(INTDIR)\parse_file_list.obj" \
	"$(INTDIR)\rexec.obj" \
	"$(INTDIR)\statistics.obj" \
	"$(INTDIR)\sysdep.obj" \
	"$(INTDIR)\timefunc.obj" \
	"$(INTDIR)\webmaster.obj"

"$(OUTDIR)\webmaster.exe" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE=$(InputPath)
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\webmaster.exe"
   copy debug\webmaster.exe ..\..\bin
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(CFG)" == "webmaster - Win32 Release" || "$(CFG)" ==\
 "webmaster - Win32 Debug"
SOURCE=..\..\src\bench.c
DEP_CPP_BENCH=\
	"..\..\src\bench.h"\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\bench.obj" : $(SOURCE) $(DEP_CPP_BENCH) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\errexit.c
DEP_CPP_ERREX=\
	"..\..\src\bench.h"\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\errexit.obj" : $(SOURCE) $(DEP_CPP_ERREX) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\getopt.c

"$(INTDIR)\getopt.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\gettimeofday.c

"$(INTDIR)\gettimeofday.obj" : $(SOURCE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\parse_file_list.c
DEP_CPP_PARSE=\
	"..\..\src\bench.h"\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\parse_file_list.obj" : $(SOURCE) $(DEP_CPP_PARSE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\rexec.c
DEP_CPP_REXEC=\
	"..\..\src\bench.h"\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\rexec.obj" : $(SOURCE) $(DEP_CPP_REXEC) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\statistics.c
DEP_CPP_STATI=\
	"..\..\src\bench.h"\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\statistics.obj" : $(SOURCE) $(DEP_CPP_STATI) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\sysdep.c
DEP_CPP_SYSDE=\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\sysdep.obj" : $(SOURCE) $(DEP_CPP_SYSDE) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\timefunc.c
DEP_CPP_TIMEF=\
	"..\..\src\bench.h"\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\timefunc.obj" : $(SOURCE) $(DEP_CPP_TIMEF) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


SOURCE=..\..\src\webmaster.c
DEP_CPP_WEBMA=\
	"..\..\src\bench.h"\
	"..\..\src\sysdep.h"\
	

"$(INTDIR)\webmaster.obj" : $(SOURCE) $(DEP_CPP_WEBMA) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)



!ENDIF 

