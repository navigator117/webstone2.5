# Microsoft Developer Studio Generated NMAKE File, Based on ws20_ns.dsp
!IF "$(CFG)" == ""
CFG=ws20_ns - Win32 Debug
!MESSAGE No configuration specified. Defaulting to ws20_ns - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "ws20_ns - Win32 Release" && "$(CFG)" !=\
 "ws20_ns - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ws20_ns.mak" CFG="ws20_ns - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ws20_ns - Win32 Release" (based on\
 "Win32 (x86) Dynamic-Link Library")
!MESSAGE "ws20_ns - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

!IF  "$(CFG)" == "ws20_ns - Win32 Release"

OUTDIR=.\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\ws20_ns.dll"

!ELSE 

ALL : "$(OUTDIR)\ws20_ns.dll"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\ws20_ns.obj"
	-@erase "$(OUTDIR)\ws20_ns.dll"
	-@erase "$(OUTDIR)\ws20_ns.exp"
	-@erase "$(OUTDIR)\ws20_ns.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MT /W3 /GX /O2 /I "d:\netscape\include" /I "d:\nsapi\include"\
 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "XP_WIN32" /Fp"$(INTDIR)\ws20_ns.pch"\
 /YX /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ws20_ns.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib d:\nsapi\examples\libhttpd.lib /nologo /subsystem:windows /dll\
 /incremental:no /pdb:"$(OUTDIR)\ws20_ns.pdb" /machine:I386\
 /out:"$(OUTDIR)\ws20_ns.dll" /implib:"$(OUTDIR)\ws20_ns.lib" 
LINK32_OBJS= \
	"$(INTDIR)\ws20_ns.obj"

"$(OUTDIR)\ws20_ns.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE=$(InputPath)
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Release
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\ws20_ns.dll"
   copy release\ws20_ns.dll ..\..\lgm
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ELSEIF  "$(CFG)" == "ws20_ns - Win32 Debug"

OUTDIR=.\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\ws20_ns.dll"

!ELSE 

ALL : "$(OUTDIR)\ws20_ns.dll"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(INTDIR)\ws20_ns.obj"
	-@erase "$(OUTDIR)\ws20_ns.dll"
	-@erase "$(OUTDIR)\ws20_ns.exp"
	-@erase "$(OUTDIR)\ws20_ns.ilk"
	-@erase "$(OUTDIR)\ws20_ns.lib"
	-@erase "$(OUTDIR)\ws20_ns.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

CPP=cl.exe
CPP_PROJ=/nologo /MTd /W3 /Gm /GX /Zi /Od /I "d:\nsapi\include" /D "_DEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "XP_WIN32" /Fp"$(INTDIR)\ws20_ns.pch" /YX\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

MTL=midl.exe
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
RSC=rc.exe
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\ws20_ns.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib\
 advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib\
 odbccp32.lib d:\nsapi\examples\libhttpd.lib /nologo /subsystem:windows /dll\
 /incremental:yes /pdb:"$(OUTDIR)\ws20_ns.pdb" /debug /machine:I386\
 /out:"$(OUTDIR)\ws20_ns.dll" /implib:"$(OUTDIR)\ws20_ns.lib" /pdbtype:sept 
LINK32_OBJS= \
	"$(INTDIR)\ws20_ns.obj"

"$(OUTDIR)\ws20_ns.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

SOURCE=$(InputPath)
DS_POSTBUILD_DEP=$(INTDIR)\postbld.dep

ALL : $(DS_POSTBUILD_DEP)

# Begin Custom Macros
OutDir=.\Debug
# End Custom Macros

$(DS_POSTBUILD_DEP) : "$(OUTDIR)\ws20_ns.dll"
   copy debug\ws20_ns.dll ..\..\lgm
	echo Helper for Post-build step > "$(DS_POSTBUILD_DEP)"

!ENDIF 


!IF "$(CFG)" == "ws20_ns - Win32 Release" || "$(CFG)" ==\
 "ws20_ns - Win32 Debug"
SOURCE="..\..\src\API-test\ws20_ns.c"

!IF  "$(CFG)" == "ws20_ns - Win32 Release"

DEP_CPP_WS20_=\
	"..\..\..\..\nsapi\include\base\buffer.h"\
	"..\..\..\..\nsapi\include\base\file.h"\
	"..\..\..\..\nsapi\include\base\net.h"\
	"..\..\..\..\nsapi\include\base\pblock.h"\
	"..\..\..\..\nsapi\include\base\pool.h"\
	"..\..\..\..\nsapi\include\base\sem.h"\
	"..\..\..\..\nsapi\include\base\session.h"\
	"..\..\..\..\nsapi\include\base\systems.h"\
	"..\..\..\..\nsapi\include\frame\object.h"\
	"..\..\..\..\nsapi\include\frame\objset.h"\
	"..\..\..\..\nsapi\include\frame\req.h"\
	"..\..\..\..\nsapi\include\netsite.h"\
	"..\..\..\..\nsapi\include\version.h"\
	

"$(INTDIR)\ws20_ns.obj" : $(SOURCE) $(DEP_CPP_WS20_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ELSEIF  "$(CFG)" == "ws20_ns - Win32 Debug"

DEP_CPP_WS20_=\
	"..\..\..\..\nsapi\include\base\buffer.h"\
	"..\..\..\..\nsapi\include\base\file.h"\
	"..\..\..\..\nsapi\include\base\net.h"\
	"..\..\..\..\nsapi\include\base\pblock.h"\
	"..\..\..\..\nsapi\include\base\pool.h"\
	"..\..\..\..\nsapi\include\base\sem.h"\
	"..\..\..\..\nsapi\include\base\session.h"\
	"..\..\..\..\nsapi\include\base\systems.h"\
	"..\..\..\..\nsapi\include\frame\object.h"\
	"..\..\..\..\nsapi\include\frame\objset.h"\
	"..\..\..\..\nsapi\include\frame\req.h"\
	"..\..\..\..\nsapi\include\netsite.h"\
	"..\..\..\..\nsapi\include\version.h"\
	

"$(INTDIR)\ws20_ns.obj" : $(SOURCE) $(DEP_CPP_WS20_) "$(INTDIR)"
	$(CPP) $(CPP_PROJ) $(SOURCE)


!ENDIF 


!ENDIF 

