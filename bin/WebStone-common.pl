#!/bin/perl
#
############################################################################
#                                                                          #
# The contents of this file are subject to the WebStone Public License     #
# Version 1.0 (the "License"); you may not use this file except in         #
# compliance with the License. You may obtain a copy of the License        #
# at http://www.mindcraft.com/webstone/license10.html                      #
#                                                                          #
# Software distributed under the License is distributed on an "AS IS"      #
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      #
# the License for the specific language governing rights and limitations   #
# under the License.                                                       #
#                                                                          #
# The Original Code is WebStone 2.5.                                       #
#                                                                          #
# The Initial Developer of the Original Code is Silicon Graphics, Inc.     #
# and Mindcraft, Inc.. Portions created by Silicon Graphics. and           #
# Mindcraft. are Copyright (C) 1995#1998 Silicon Graphics, Inc. and        #
# Mindcraft, Inc. All Rights Reserved.                                     #
#                                                                          #
# Contributor(s): ______________________________________.                  #
#                                                                          #
# @(#) WebStone-common.pl 2.7@(#)
############################################################################


1;

sub show_model {
    $model = &my_head("$wd/conf/filelist");
    $model =~ s/\#//;
    ( $model ) = split(':', $model);

    print CLIENT <<EOF
<P><STRONG>
<A HREF=\"$wd/conf/filelist\">$model
</A></STRONG>
EOF
    ;
}

sub my_head {
    my($firstline);
    local *HFD;

    open(HFD, "< $_[0]") or return '';
    $firstline = <HFD>;
    close HFD;
    $firstline;
}

sub html_begin {

    ( $title ) = @_;

    close(STDOUT);
    open(STDOUT, ">&CLIENT");
    close(STDERR);
    open(STDERR, ">&CLIENT");

    print CLIENT <<EOF
<HTML>
<HEAD>
<TITLE>WebStone $title</TITLE>
<BODY BGCOLOR="#FFFFF8">
<A HREF="$wd/doc/WebStone-gui.html">
<IMG SRC="$wd/doc/webstone.gif" ALT="WebStone" BORDER=0 ></A>
<H1>World Wide Web Server Benchmarking</H1>
<DL>
<DT><EM>If you have any questions, please read the 
<A HREF="$wd/doc/FAQ-webstone.html">WebStone FAQ</A>.</EM>
<HR>
EOF
    ;

}

sub html_end {

    print CLIENT <<EOF
<HR>
<ADDRESS><A HREF="$wd/doc/LICENSE.html">Copyright &copy; 1995 Silicon Graphics Inc.<BR>Copyright &copy; 1997 Mindcraft, Inc.</A>
</ADDRESS>
</BODY>
</HTML>
EOF
    ;

    close(STDERR);
    close(STDOUT);
    open(STDOUT);
    open(STDERR);
}

# end
