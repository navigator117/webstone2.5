#!/bin/sh
############################################################################
#                                                                          #
# The contents of this file are subject to the WebStone Public License     #
# Version 1.0 (the "License"); you may not use this file except in         #
# compliance with the License. You may obtain a copy of the License        #
# at http://www.mindcraft.com/webstone/license10.html                      #
#                                                                          #
# Software distributed under the License is distributed on an "AS IS"      #
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      #
# the License for the specific language governing rights and limitations   #
# under the License.                                                       #
#                                                                          #
# The Original Code is WebStone 2.5.                                       #
#                                                                          #
# The Initial Developer of the Original Code is Silicon Graphics, Inc.     #
# and Mindcraft, Inc.. Portions created by Silicon Graphics. and           #
# Mindcraft. are Copyright (C) 1995#1998 Silicon Graphics, Inc. and        #
# Mindcraft, Inc. All Rights Reserved.                                     #
#                                                                          #
# Contributor(s): ______________________________________.                  #
#                                                                          #
############################################################################

# get configuration
[ -n "$WEBSTONEROOT" ] || WEBSTONEROOT=`pwd`/..
. $WEBSTONEROOT/conf/testbed

if [ ! -d "$TMPDIR" ]
then
	TMPDIR="/tmp"
fi

case $# in
  1)
     FILELIST=$1
  *)
     FILELIST=$WEBSTONEROOT/conf/filelist
     ;;
esac

TIMESTAMP=`date +"%y%m%d%H"`
TIMESTAMP=$TIMESTAMP`date +"%M"`
trap "rm -rf $TMPDIR/webstone-genfiles.$TIMESTAMP" 0 1 2 3 10 15
mkdir $TMPDIR/webstone-genfiles.$TIMESTAMP
cd $TMPDIR/webstone-genfiles.$TIMESTAMP

cat $FILELIST |
nawk '
($3 ~ /^\#[0-9]*/) {
  sub(/^\#/, "", $3);
  cmd = WEBSTONEROOT "/bin/genrand " $3 " ."$1
  print cmd;
  system(cmd);
  cmd = RCP " ." $1 " " SERVER ":" WEBDOCDIR
  print cmd;
  system(cmd);
  cmd = "rm -f ." $1
  print cmd;
  system(cmd);
  next
}
' $* WEBSTONEROOT=$WEBSTONEROOT RCP=$RCP SERVER=$SERVER WEBDOCDIR=$WEBDOCDIR

#end
