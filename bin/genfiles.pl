#! /usr/local/bin/perl
############################################################################
#                                                                          #
# The contents of this file are subject to the WebStone Public License     #
# Version 1.0 (the "License"); you may not use this file except in         #
# compliance with the License. You may obtain a copy of the License        #
# at http://www.mindcraft.com/webstone/license10.html                      #
#                                                                          #
# Software distributed under the License is distributed on an "AS IS"      #
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      #
# the License for the specific language governing rights and limitations   #
# under the License.                                                       #
#                                                                          #
# The Original Code is WebStone 2.5.                                       #
#                                                                          #
# The Initial Developer of the Original Code is Silicon Graphics, Inc.     #
# and Mindcraft, Inc.. Portions created by Silicon Graphics. and           #
# Mindcraft. are Copyright (C) 1995#1998 Silicon Graphics, Inc. and        #
# Mindcraft, Inc. All Rights Reserved.                                     #
#                                                                          #
# Contributor(s): ______________________________________.                  #
#                                                                          #
############################################################################

# @(#) genfiles.pl 1.3@(#)
use Cwd;
$wd = cwd;
push(@INC, "$wd/bin");
require('ws-utils.pl');
use Env;

# get configuration
&set_webstone_conf();

unless (-d $GENFILES_DIR)
{
	mkdir($GENFILES_DIR, 0777) or die "Creating directory '$GENFILES_Dir': $!";
}

print "Creating files in directory '$GENFILES_DIR'\n";

open(FILELIST, "$WEBSTONEROOT/conf/fileset") or 
	die "Opening URL file $WEBSTONEROOT/conf/fileset: $!";
while(<FILELIST>)
{
	next if /^\s*$/ or /^\s*#/;	# omit blank and comment lines
	chop;
	s/\s*#.*$//;		# remove comments
	($filesize, $filename) = split /\s+/;
	
	$cmd = "$GENRAND $filesize $GENFILES_DIR/$filename";
	print "$cmd\n";
	system ($cmd) and print "Error from system($cmd)\n";
}
close FILELIST;

print <<EOF;
Done!

Now copy or move the files from the directory named '$GENFILES_DIR'
to the root of your web server document tree.
EOF
exit 0;

