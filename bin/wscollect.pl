#!/usr/local/bin/perl
############################################################################
#                                                                          #
# The contents of this file are subject to the WebStone Public License     #
# Version 1.0 (the "License"); you may not use this file except in         #
# compliance with the License. You may obtain a copy of the License        #
# at http://www.mindcraft.com/webstone/license10.html                      #
#                                                                          #
# Software distributed under the License is distributed on an "AS IS"      #
# basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      #
# the License for the specific language governing rights and limitations   #
# under the License.                                                       #
#                                                                          #
# The Original Code is WebStone 2.5.                                       #
#                                                                          #
# The Initial Developer of the Original Code is Silicon Graphics, Inc.     #
# and Mindcraft, Inc.. Portions created by Silicon Graphics. and           #
# Mindcraft. are Copyright (C) 1995#1998 Silicon Graphics, Inc. and        #
# Mindcraft, Inc. All Rights Reserved.                                     #
#                                                                          #
# Contributor(s): ______________________________________.                  #
#                                                                          #
############################################################################

# updated version of the old wscollect script which goes through
# webstone run directories and summarizes the output in tabular
# format.
# syc 4/25/96
#

require "find.pl";
use Cwd;

#
# the list @runs contains the timestamps for the runs which are found
# during the traversal of the runs directory. This list is used for
# indices into the associative arrays for storing run information.
# 
# $numclients{ $time }    - number of clients for the run
# $connrate{ $time }      - connection rate average
# $littlesload{ $time }   - little's load factor
# $latency{ $time }       - latency average
# $error{ $time }         - error rate
# $throughput{ $time }    - throughput

local( @runs,
       %numclients,
       %connrate,
       %littlesload,
       %latency,
       %error,
       %throughput);

# Got rid of the trick hack of the title names, someone can put it
# back in later
@title = ( "Timestamp",
	  "Total number of clients",
	  "Server connection rate",
	  "Little's Load Factor",
	  "Average response time",
	  "Error Level",
	  "Average client thruput");


sub wanted {
    local( $filename ) = $_;

    return unless ( $filename =~ /run/ );

    local( $instats) = 0;
    local( $runtime, $tag, $data, $cruft, @cruft );

    open( FILE, $filename ) || return; # bail if failed to open
    Cwd::getcwd() =~ /([\d_]+$)/;
    $runtime = $1;
    push( @runs, $runtime);
    while ( $line = <FILE>) {
	if (! $instats) {
	    $instats = 1 if ( $line =~ /^WEBSTONE 2\..\S* results/ );
	    next;
	}
	chop( $line );
	( $tag, $data ) = split( /:?\s{2,}|\t/, $line);

	# perl hack to emulate case/switch statement
	$tag =~ /number of clients/ &&
	    ($numclients{ $runtime } = $data, next);
        $tag =~ /error rate/ &&
	    (( $error{ $runtime }) = $data =~ /([\d\.]+)/, next);
	$tag =~ /connection rate/ &&
	    (( $connrate{ $runtime }) = $data =~ /([\d\.]+)/, next);
	$tag =~ /Server thruput/ &&
	    (( $throughput{ $runtime }) = $data =~ /([\d\.]+)/, next);
	$tag =~ /Little's Load/  && 
            (( $littlesload{ $runtime}) = $data =~ /([\d\.]+)/, next); # '
        $tag =~ /Average response time/ &&
	    (( $latency{ $runtime } ) = $data =~ /([\d\.]+)/, next);
    }
    close( FILE );
    unless ( $throughput{ $runtime} )  {
	pop( @runs); # if we didn't get a throughput, then the
				# data is incomplete and just drop this run
    }
}

# End
