#! /usr/local/bin/perl

$thesize = 0;

print STDOUT "<DL>\n";
open(HELPFILE, "testbed.help");
while (<HELPFILE>) {
	if (/^\#|^(\w)*$/) { # do nothing
	}
	else {
		( $textvalue, $thevalue ) = split( ':', $_ );
		( $thevalue ) = split( '#', $thevalue);	    
		$newsize = length($textvalue);
		$thesize = ( $thesize > $newsize ) ? $thesize : $newsize ;
		$testbed{$textvalue} = $thevalue;
		push(@keylist, $textvalue);
	}
}
close(HELPFILE);

foreach $key (@keylist) {
	print STDOUT "<P><DT><STRONG>$key</STRONG>";
	print STDOUT "<DD>$testbed{$key}</DD>\n";
}
print STDOUT "</DL>\n";
