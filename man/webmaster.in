define(MC_TITLE,webmaster)
define(MC_DATE,98/05/14)
include(mc_html_head.h)
<!-- @(#) webmaster.in 1.5@(#) -->
</UL><FONT SIZE=+1><STRONG>NAME</STRONG></FONT><UL>
webmaster - WebStone benchmark control program
</UL><FONT SIZE=+1><STRONG>SYNOPSIS</STRONG></FONT><UL>
<STRONG>webmaster</STRONG>
-f config_file
[ -adrsvRSTWX ]
[ -C webclient_path ]
[ -D debugfile ]
[ -M netmask ]
[ -P proxy_server ]
[ -U clientfilelist ]
[ -p port_num ]
[ -t run_time | -l loop_count ]
[ -u masterfilelist ]
[ -w webserver ]
[ URL ... ]
</UL><FONT SIZE=+1><STRONG>DESCRIPTION</STRONG></FONT><UL>
<P>
<STRONG>webmaster</STRONG>
is the program that manages the execution of a WebStone benchmark run.
It uses
<STRONG>rexec(3N)</STRONG>
to invoke a
<STRONG>webclient(1)</STRONG>
process on each of the client load-generating systems to be used for the
benchmark.
<P>
When the benchmark run is done,
<STRONG>webmaster</STRONG>
polls the
<STRONG>webclient</STRONG>
processes for the data they've collected, summarizes the data,
and writes it to standard output.
</UL><FONT SIZE=+1><STRONG>OPTIONS</STRONG></FONT><UL>
<DL>
<DT><STRONG>-C webclient_path</STRONG><DD>
Absolute pathname of the
<STRONG>webclient</STRONG>
program, on the client system(s).
<DT><STRONG>-D debugfile</STRONG><DD>
File to receive debug data, on the client system(s).
<DT><STRONG>-M netmask</STRONG><DD>
The netmask value to be used in choosing which network interface on the
server to use.  Default: 255.255.255.0.
<DT><STRONG>-P proxy_server</STRONG><DD>
The name of a proxy server to use for requests.
<DT><STRONG>-R</STRONG><DD>
Tell \fBwebclient\fR to record all transactions.
<DT><STRONG>-S</STRONG><DD>
Use a fixed seed for the random number generator that chooses which page
to request.
<DT><STRONG>-T</STRONG><DD>
Turn on trace statements.
<DT><STRONG>-U URL_file</STRONG><DD>
Take load-generating URLs from URL_file on the client system.
<DT><STRONG>-W</STRONG><DD>
Web server addresses are in the config file ("-f" parameter)
on the webmaster system.
<DT><STRONG>-X</STRONG><DD>
Don't use
<STRONG>rexec()</STRONG>
to start the
<STRONG>webclient</STRONG>
processes.  Instead,
<STRONG>sleep()</STRONG>
for 30 seconds to allow the user to start them by hand.
<DT><STRONG>-a</STRONG><DD>
Print timing information for all clients.
<DT><STRONG>-d</STRONG><DD>
Turn on debug and trace statements.
<DT><STRONG>-f config_file</STRONG><DD>
File specifying clients.  The file must contain one line for each
client system, with the syntax:
<DT><STRONG></STRONG><DD>
<P>
<UL>
&lt;client host name&gt; &lt;login&gt; &lt;password&gt; &lt;number of webclients&gt; [&lt;Web server&gt;]
</UL>
<P>
<DT><STRONG></STRONG><DD>
It is perfectly all right for the system on which
<STRONG>webmaster</STRONG>
runs to also be a client system.
<DT><STRONG>-l loop_count</STRONG><DD>
Cycle through the filelist "loop_count" times, requesting each URL from the
server in turn.  Conflicts with "-t".
<DT><STRONG>-p portnum</STRONG><DD>
Port number on which the Web server runs.  Default: 80.
<DT><STRONG>-s</STRONG><DD>
Tell
<STRONG>webclient</STRONG>
to save all data retrieved from the Web server.
<DT><STRONG>-t run_time</STRONG><DD>
Duration of the test, in minutes.  Conflicts with "-l".
<DT><STRONG>-u URL_file</STRONG><DD>
File containing URLs to use for the test, on the webmaster system.
<DT><STRONG>-v</STRONG><DD>
Enable verbose mode.
<DT><STRONG>-w webserver</STRONG><DD>
Host name of the Web server to test.  If the name starts with a digit,
<STRONG>webmaster</STRONG>
uses it as the host part of an IP number and uses the netmask ("-M"
option) to construct a host name on the same net as the client
system.
<P>
For example, if the client system's IP address is 168.10.2.14, the
netmask has the default value (255.255.255.0), and
<EM>
webserver
</EM>
is 25,
<STRONG>webmaster</STRONG>
tells the client system's
<STRONG>webclient</STRONG>
to use 168.10.2.25 for its Web server.
<P>
</UL><FONT SIZE=+1><STRONG>DETAILS</STRONG></FONT><UL>
<P>
<STRONG>webmaster</STRONG>
is the program that manages a single measurement run of the WebStone
benchmark.  It parses all the configuration information for the run,
opens a socket to communicate with the client systems that will generate
load on the server, and calls
<STRONG>rexec()</STRONG>
to start
<STRONG>webclient</STRONG>
on each client system.
<P>
At the end of the run
<STRONG>webmaster</STRONG>
polls the
<STRONG>webclient</STRONG>
processes to receive their results.  It then summarizes the results
and writes them to the standard output stream.
<P><STRONG>Web Server</STRONG>
There are a number of ways to specify the Web server to be tested:
<UL>
<P>
<LI>With the "-w webserver" command line parameter.
<P>
<LI>On the client's line in the config file, as set with the
"-f config_file" command line parameter.  The value from the config
file is not used unless the "-W" command line parameter is given.
<P>
<LI>In the URL list.  If a full URL is given, it is honored.  If
the string in the list is a file name beginning with a slash, the
server name from the command line or from the config file is prepended.
<P>
If a proxy server is to be tested, each URL in the list must be a
full URL that specifies the server name.
</UL>
<P><STRONG>Test Load</STRONG>
There are a number of ways to specify the list of URLs that
<STRONG>webmaster</STRONG>
is to provide to its
<STRONG>webclient</STRONG>
slaves:
<UL>
<P>
<LI>Use the "-u URL_list" option to provide the name of a file on the
webmaster system from which
<STRONG>webmaster</STRONG>
is to read the URLs.
<P>
<LI>List the URLs on the
<STRONG>webmaster</STRONG>
command line.
<P>
<LI>Give the URLs and weights on the standard input stream.
<P>
<LI>Use the "-U URL_list" option to tell the
<STRONG>webclient</STRONG>
to read the load URLs from a file on the client system.
</UL>
<P>
Unless the "-U URL_list" option is used,
<STRONG>webmaster</STRONG>
provides the list of URLs to
<STRONG>webclient</STRONG>
over the master socket.
<STRONG>webclient</STRONG>
then parses the URL list and returns the number of entries to
<STRONG>webmaster</STRONG>
on the same socket.
<P>
See
<STRONG>webclient(1)</STRONG>
for a detailed description of load generation.
<P>
</UL><FONT SIZE=+1><STRONG>FILES</STRONG></FONT><UL>
<P>
<STRONG>webmaster</STRONG>
writes its output to its standard output stream.
<P>
Other files used by
<STRONG>webmaster</STRONG>
are specified on the command line.
If no URL files are specified on the command line,
<STRONG>webmaster</STRONG>
tries to read the list of URLs from its standard input stream.
<P>
During a test run
<STRONG>webmaster</STRONG>
keeps a socket open to each
<STRONG>webclient</STRONG>
process.  Make sure that the number of file descriptors available
to
<STRONG>webmaster</STRONG>
is greater than the total number of
<STRONG>webclient</STRONG>
processes to be run.
The "-R" and "-s" flags also each cause one file per
<STRONG>webclient</STRONG>
process to be opened on the client system.
<P>
<STRONG>File format for load URL lists:</STRONG>
<P>
For each URL to be used to generate the test load, the URL list
has an entry with the following form.  Comments and blank lines in
the list are ignored.  Each comment starts with a hash character ('#')
and continues to the end of the line.
<P>
<UL>
<TT>
&lt;URL&gt; &lt;weight&gt; [#&lt;comment&gt;]
</TT>
</UL>
<P>
</UL><FONT SIZE=+1><STRONG>ENVIRONMENT</STRONG></FONT><UL>
<P>
<STRONG>webmaster</STRONG>
does not use any environment variables.
<P>
</UL><FONT SIZE=+1><STRONG>AUTHOR</STRONG></FONT><UL>
include(copyright.h)
<P>
</UL><FONT SIZE=+1><STRONG>LIMITATIONS</STRONG></FONT><UL>
<P>
The command line options are too complicated.
<STRONG>webmaster</STRONG>
should usually be run by scripts.
<P>
</UL><FONT SIZE=+1><STRONG>SEE ALSO</STRONG></FONT><UL>
<P>
<A href=runbench.html>runbench(1)</A>,
<A href=webclient.html>webclient(1)</A>,
<A href=webstone.html>webstone(1)</A>,
<A href=webstonegui.html>webstonegui(1)<A>
include(mc_html_foot.h)
