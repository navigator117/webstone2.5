/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 ***************************************************************************/

/*
 * Send random bits file
 * Once this service function is installed, any file with the extension
 * "dyn-send" will be serviced with this function.  An optional query
 * string may be passed to alter the amount of data in the response.
 *
 * For example:
 *      /file.dyn-send                  - returns a 10240 byte file
 *      /file.dyn-send?size=20          - returns a 20 byte file
 *      /file.dyn-send?size=1024        - returns a 1024 byte file
 *      etc.
 *
 * To install the service routine, compile it as per the makefile
 * included with your Netscape server distribution (serverroot/nsapi/examples)
 * and then add the following lines to your netscape server configuration:
 *
 * in magnus.conf
 *      Init fn=load-modules shlib=example.so funcs=nsapi-send
 *
 * in obj.conf
 *      Service method=(GET|HEAD) fn=nsapi-send type=magnus-internal/dyn-send
 *
 * in mime.types
 *      type=magnus-internal/dyn-send        exts=dyn-send
 *
 * Mike Belshe
 * mbelshe@netscape.com
 * 11-5-95
 *
 * NOTE: 
 *    This module contains two bugs: 
 *        1) The array "buffer" is declared with large size (~204800).
 *           As a result, the web server may crash due to lack of memory.
 *        2) If the requested size is greater than 204800 (which it is for
 *	     the standard WebStone file set) than this module may crash the
 *           web server by overwriting the end of the buffer array.
 *    We are leaving these two bugs in the code because to fix them would
 *    change the performance behaviour of this module.
 *
 *    Greg Burrell
 *    Mindcraft, Inc.
 */

#ifndef WIN32
#include <stdio.h>
#include <stdlib.h>
#include "base/pblock.h"
#include "base/session.h"
#include "frame/protocol.h"
#include "base/util.h"
#include "frame/http.h"
#else
#include <windows.h>
#define FILE_STDIO 1
#endif
#include "frame/req.h"

#define FILE_SIZE       10240
#define HEADERS  "HTTP/1.0 200 OK\r\nContent-type: text/html\r\n\r\n"

#ifdef WIN32
__declspec(dllexport) 
#endif

int nsapi_send(pblock *pb, Session *sn, Request *rq)
{
    char *query_string;
    char buffer[sizeof(HEADERS) + 204800 + 1];
    int filesize;
    unsigned int maxindex;
    unsigned int index;

    /* Get the query string, if any; check to see if an alternate
     * file size was specified.
     */
    if ( !(query_string = pblock_findval("query", rq->reqpb)) )
	filesize = FILE_SIZE;
    else {
	filesize = atoi(&(query_string[5]));
    }

    memcpy(&buffer, HEADERS, sizeof(HEADERS)-1);

    /* Generate the output */
    maxindex = sizeof(HEADERS) + filesize;
    for (index=sizeof(HEADERS); index < (maxindex); index++)
	/* generate random characters from A-Z */
#ifdef IRIX
	buffer[index] = rand_r(&maxindex) % 26 + 63;
#else
        buffer[index] = rand() %26 + 63;
#endif

    /* Send the output */
    if (net_write(sn->csd, buffer, sizeof(HEADERS)-1+filesize) == IO_ERROR)
	return REQ_EXIT;

    return REQ_PROCEED;
}
