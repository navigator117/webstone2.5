/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 ***************************************************************************/


/*
 * ws25_iis.c 1.4
 *
 * NAME
 *	ws_isapi_25.c
 *
 * PURPOSE
 *      A module for use with the WebStone test suite to measure performance
 *	of MicroSoft Internet Information Server using ISAPI.  This module 
 *	will send back the contents of a given file.
 *
 * USAGE:
 *     Once this service function is installed, it can be used in a URL just
 *     like a normal CGI script.  The argument to the URL is the file name.  
 *     For example:
 *
 *         /cgi-bin/ws_isapi_25.dll?file=D:\wwwroot\file500.html
 *
 *     To install the service routine, compile it as per the makefile
 *     and place it in the web server's CGI bin directory.
 */
#include <windows.h>
#include <httpext.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#ifndef BUFSIZE
#define BUFSIZE	4096
#endif

#define CONTENT_TYPE "Content-type: text/plain\r\n\r\n"

#define USAGE33 "<pre>"\
		  "URLs used with this module should have the form:\n\n"\
		  "     /cgi-bin/ws_isapi_25.dll?file=&lt;path&gt;\n\n"\
		  "where <i>&lt;path&gt;</i> is a file whose contents will be returned.\n"\
	   	  "For example:\n\n"\
		  "    /cgi-bin/ws_isapi_25.dll?file=d:\\wwwroot\\file500.html\r\n"
#define USAGE "<br>"\
		  "URLs used with this module should have the form:<br><br>\n\n"\
		  "     /cgi-bin/ws_isapi_25.dll?file=path<br><br>\n\n"\
		  "where 'path' is a file whose contents will be returned.<br>\n"\
	   	  "For example:<br><br>\n\n"\
		  "    /cgi-bin/ws_isapi_25.dll?file=d:\\wwwroot\\file500.html\r\n"

DWORD WINAPI
HttpExtensionProc( IN OUT EXTENSION_CONTROL_BLOCK * pecb)
{
	char	*query_string;
	char	*filename;
	FILE	*fp;
	char	buf[BUFSIZE];
	DWORD	nread;

    	/*
	 * Get the file name from the request.
	 */
	if ( !(query_string = pecb->lpszQueryString) ||
		strncmp(query_string, "file=", 5) ||
		(*(filename = &(query_string[5])) == '\0')
	   )
      {
		(void) pecb->ServerSupportFunction( pecb->ConnID,
                                      HSE_REQ_SEND_RESPONSE_HEADER,
                                      "400 Bad Request",
                                      NULL,
                                      (LPDWORD) CONTENT_TYPE "Bad Request" USAGE);
		return HSE_STATUS_ERROR;
	}

	if ((fp = fopen(filename, "r")) == NULL)
	{
		(void) pecb->ServerSupportFunction( pecb->ConnID,
                                      HSE_REQ_SEND_RESPONSE_HEADER,
                                      "500 Unable to open file",
                                      NULL,
                                      (LPDWORD) CONTENT_TYPE "Unable to open file" USAGE);
		return HSE_STATUS_ERROR;
	}

	/* 
	 * So far so good, we're ready to start sending the file back.
	 * Set content-type and begin a "success" response. 
	 */
	if ( !pecb->ServerSupportFunction( pecb->ConnID,
                                      HSE_REQ_SEND_RESPONSE_HEADER,
                                      "200 OK",
                                      NULL,
                                      (LPDWORD) CONTENT_TYPE))
	{
		return HSE_STATUS_ERROR;
	}

	while (nread = (DWORD) fread(buf, sizeof(char), BUFSIZE, fp))
	{
		if (!pecb->WriteClient(pecb->ConnID, buf, &nread, 0))
		{
			(void) fclose(fp);
			return HSE_STATUS_ERROR;
		}
	}
	if (ferror(fp))
	{
		(void) fclose(fp);
		return HSE_STATUS_ERROR;
	}

	/*
	 * Success!
	 */
	(void) fclose(fp);
	return HSE_STATUS_SUCCESS;
} // HttpExtensionProc()


BOOL WINAPI
GetExtensionVersion(
    HSE_VERSION_INFO * pver
    )
{
    pver->dwExtensionVersion = MAKELONG( 1, 0 );
    strcpy( pver->lpszExtensionDesc,
            "WebStone 2.5 ISAPI module" );

    return TRUE;
}



