/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 ***************************************************************************/


/*
 * Send 10K file; send random bits.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILE_SIZE       10240
#define MALLOC_FAILURE  "Out of memory"

int
main()
{
        int filesize;
        char *str_filesize;
        char *buffer;
        int index;

        printf("Content-type: text/plain\n\n");

        if ( !(str_filesize = getenv("QUERY_STRING")) )
                filesize = FILE_SIZE;
        else {
                if ( !strncmp(str_filesize, "size=", 5) )
                        filesize = atoi(&(str_filesize[5]));
                else
                        filesize = FILE_SIZE;
        }

        if ( !(buffer = (char *)malloc(filesize)) ) {
                fwrite(MALLOC_FAILURE, strlen(MALLOC_FAILURE), 1, stdout);
                return -1;
        }

        for (index=0; index< filesize; index++)
                /* generate random characters from A-Z */
                buffer[index] = rand() %26 + 63;

        fwrite(buffer, filesize, 1, stdout);

	free(buffer);

        return 0;
}
