/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 ***************************************************************************/

/****************************************************************************** 
 * ws25_cgi.c 1.3
 *
 * WebStone 2.5, CGI module
 *---------------------------------------------------------------------------
 * DESCRIPTION:
 *     Once this service function is installed, any file with the extension
 *     "cgi-send" will be serviced with this function.  A query string 
 *     specifies a file whose contents will be sent back as the reply body.
 *
 *---------------------------------------------------------------------------
 * EXAMPLE:
 *      /file.cgi-send?file=/file500.html  - returns the contents of the file
 *                                           "file500.html" found at the top
 *                                           of the web server document tree.
 *
 *---------------------------------------------------------------------------
 * BUILD:
 *    Use the make file provided with this module.  You may have to modify
 *    so of the definitions; instructions are included in the make file.
 *
 *---------------------------------------------------------------------------
 * INSTALLATION:
 *XXX
 *    in obj.conf
 *        Service method=(GET|HEAD) fn=nsapi-send type=magnus-internal/dyn-send
 *
 *    in mime.types
 *          type=magnus-internal/dyn-send        exts=dyn-send
 *
 *---------------------------------------------------------------------------
 * CHANGE LOG:
 *     2/11/97	written by
 *              Greg Burrell (greg@mindcraft.com) of Mindcraft, Inc.
 *		
 *****************************************************************************/
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>

void
main()
{
	char *query_string;
	char *filename;
	char buffer[BUFSIZ];
	FILE *fp;
	size_t nread;

        printf("Content-type: text/plain\n\n");

	/*
	 * Get and check our argument.
	 */
        query_string = getenv("QUERY_STRING");
        if ((query_string == NULL) || (query_string[0] == '\0'))
	{
		printf("ERROR!\nQUERY_STRING not found in environment.\n");
		exit(1);
	}

	if (strncmp(query_string, "file=", 5))
	{
		printf("ERROR!\nMissing 'file=<filename>' argument.\n");
		exit(1);
        }

	filename = &(query_string[5]);
	if (filename[0] == '\0')
	{
		printf("ERROR!\nMissing file name argument.\n");
		exit(1);
        }

	/*
	 * Now open the file, read it, and print its contents to stdout.
	 */
	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		printf("Unable to open file '%s'\n", filename);
		exit(1);
	}

	while(nread = fread(buffer, sizeof(char), BUFSIZ, fp))
		fwrite(buffer, sizeof(char), nread, stdout);
	if (ferror(fp))
	{
		printf("Error while reading file '%s'\n", filename);
		exit(1);
	}

	fclose(fp);
	exit(0);	/* successful exit */
}
