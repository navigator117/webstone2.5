/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 * @(#) bench.c 2.4@(#)                                                     *
 ***************************************************************************/

/* errexit call for general error handling */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#ifndef WIN32
#include <errno.h>
#include <netdb.h>
#include <unistd.h>
#endif /* WIN32 */
#include <stdarg.h>
#include <sys/types.h>

#include <fcntl.h>

#include "sysdep.h"
#include "bench.h"

#ifdef HAVE_VPRINTF
#define	VPRINTF(stderr, format, args)	vfprintf((stderr), (format), (args))
#else
#ifdef HAVE_DOPRNT
#define VPRINTF(stderr, format, args)	_doprnt((format), (args), (stderr))
#endif /* HAVE_DOPRNT */
#endif /* HAVE_VPRINTF */

/* print an error message and exit 1 */
void
errexit(const char *format, ...)
{
	va_list args;
	char hostname[64] = "";
	pid_t PID;

    PID = getpid();
    gethostname(hostname, sizeof(hostname));
    fprintf(stderr, "%s PID %d: ", hostname, PID);
	DEBUGGING_ANY && fprintf(debugfile, "errexit(): entering\n");

    va_start(args, format);
    VPRINTF(stderr, format, args);
	DEBUGGING_ANY && VPRINTF(debugfile, format, args);
    va_end(args);
    fflush(stderr);
    exit(1);
}
/* that's it */

/* print an error message and return -1 */
int
returnerr(const char *format, ...)
{
va_list args;
char hostname[64] = "";
pid_t PID;

    PID = getpid();
    gethostname(hostname, sizeof(hostname));
    fprintf(stderr, "%s PID %d: ", hostname, PID);

    va_start(args, format);
    VPRINTF(stderr, format, args);
    DEBUGGING_ANY && VPRINTF(debugfile, format, args);
    va_end(args);
    fflush(stderr);
	TRACE("returnerr(): returning error\n");
    DEBUGGING_ANY && fflush(debugfile);
    return(-1);
}
/* that's it */

/* print a debug message and then flush */
int
d_printf(const char *format, ...)
{
	va_list args;
	
	va_start(args, format);
	VPRINTF(debugfile, format, args);
	va_end(args);

	fflush(debugfile);
	return 0;
}


/* returns the last network error as a string */
char *neterrstr(void) {
static char buf[200];

#ifdef WIN32
    sprintf(buf, "WSAGetLastError() = %d", WSAGetLastError());
    WSASetLastError(0);
#else
    sprintf(buf, "errno = %d: %s", errno, strerror(errno));
    errno = 0;
#endif /* WIN32 */
    return buf;
}
