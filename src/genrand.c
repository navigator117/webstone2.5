/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 * @(#) bench.c 2.4@(#)                                                     *
 ***************************************************************************/

#include <stdio.h>
#include <fcntl.h>
#include <math.h>

#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "sysdep.h"

void
main(const int argc, char* argv[])
{
    FILE* file;
    int i;
    int my_random;
    int size;
    char *cp;

    if (argc != 3) {
	printf("usage: %s file_size_in_bytes[K|M] name\n", argv[0]);
	exit(2);
    }

    if ((file = fopen(argv[2], "w")) == NULL) {
	perror("fopen");
	exit(1);
    }

    size = atoi(argv[1]);
    for (cp = argv[1]; *cp; cp++) {
	switch(*cp) {
	    case 'k':
	    case 'K':
		size *= 1024;
		break;
	    case 'm':
	    case 'M':
		size *= 1024*1024;
		break;
	}
    }

    for (i = 0; i < size; i++) {
	my_random = ((RANDOM() % 94) + 33);
	fputc((char)my_random, file);
    }

    fclose(file);
}
