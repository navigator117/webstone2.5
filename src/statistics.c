/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 * @(#) bench.c 2.4@(#)                                                     *
 ***************************************************************************/


#include <math.h>
#include <stdlib.h>
#include "sysdep.h"
#include "bench.h"


double
mean(const double sum, const int n)
{
  if (n)
    {
      return(sum / n);
    }
  else
    {
      return(0);
    }
}


double
variance(const double sum, const double sumofsquares, const int n)
{
	double	meanofsum;

	meanofsum = mean(sum, n);

	return (mean(sumofsquares,n) - (meanofsum * meanofsum));
}


double
stddev(const double sum, const double sumofsquares, const int n)
{
	return(sqrt(fabs(variance(sum, sumofsquares, n))));
}
