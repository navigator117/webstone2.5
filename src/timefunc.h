/****************************************************************************
 *                                                                          *
 * The contents of this file are subject to the WebStone Public License     *
 * Version 1.0 (the "License"); you may not use this file except in         *
 * compliance with the License. You may obtain a copy of the License        *
 * at http://www.mindcraft.com/webstone/license10.html                      *
 *                                                                          *
 * Software distributed under the License is distributed on an "AS IS"      *
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      *
 * the License for the specific language governing rights and limitations   *
 * under the License.                                                       *
 *                                                                          *
 * The Original Code is WebStone 2.5.                                       *
 *                                                                          *
 * The Initial Developer of the Original Code is Silicon Graphics, Inc.     *
 * and Mindcraft, Inc.. Portions created by Silicon Graphics. and           *
 * Mindcraft. are Copyright (C) 1995-1998 Silicon Graphics, Inc. and        *
 * Mindcraft, Inc. All Rights Reserved.                                     *
 *                                                                          *
 * Contributor(s): ______________________________________.                  *
 *                                                                          *
 ***************************************************************************/

#ifndef __TIMEFUNC_H__
#define __TIMEFUNC_H__

extern double	timevaldouble(struct timeval *);
extern void	doubletimeval(const double, struct timeval *);

extern void	addtime(struct timeval *, struct timeval *);
extern void	compdifftime(struct timeval *, struct timeval *, struct timeval *);
extern void	mintime(struct timeval *, struct timeval *);
extern void	maxtime(struct timeval *, struct timeval *);
extern void	avgtime(struct timeval *, int, struct timeval *);
extern void	variancetime(struct timeval *, double, int, struct timeval *);
extern void	stddevtime(struct timeval *, double, int, struct timeval *);

extern void	sqtime(struct timeval *, struct timeval *);

extern double	thruputpersec(const double, struct timeval *);

#endif /* !__TIMEFUNC_H__ */
