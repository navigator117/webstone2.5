:: # @(#) webstone 2.1@(#)
:: ############################################################################
:: #                                                                          #
:: # The contents of this file are subject to the WebStone Public License     #
:: # Version 1.0 (the "License"); you may not use this file except in         #
:: # compliance with the License. You may obtain a copy of the License        #
:: # at http://www.mindcraft.com/webstone/license10.html                      #
:: #                                                                          #
:: # Software distributed under the License is distributed on an "AS IS"      #
:: # basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See      #
:: # the License for the specific language governing rights and limitations   #
:: # under the License.                                                       #
:: #                                                                          #
:: # The Original Code is WebStone 2.5.                                       #
:: #                                                                          #
:: # The Initial Developer of the Original Code is Silicon Graphics, Inc.     #
:: # and Mindcraft, Inc.. Portions created by Silicon Graphics. and           #
:: # Mindcraft. are Copyright (C) 1995#1998 Silicon Graphics, Inc. and        #
:: # Mindcraft, Inc. All Rights Reserved.                                     #
:: #                                                                          #
:: # Contributor(s): ______________________________________.                  #
:: #                                                                          #
:: ############################################################################

@echo off
:gui
if not "%1" == "-gui" goto help
	echo The '-gui' option is not supported in this release.
	:: #bin\webstone-gui.pl
	goto end
:help 
if not "%1" == "-help" goto setup
	echo
	more < QUICKSTART
	goto end
:setup
if not "%1" == "-setup" goto kill
	echo The '-setup' option is not supported in this release.
	:: #bin\gui-configure
	goto end
:kill
if not "%1" == "-kill" goto genfiles
	echo The '-kill' option is not supported in this release.
	goto end
:genfiles
if not "%1" == "-genfiles" goto silent
	perl bin\genfiles.pl
	goto end
:silent
if not "%1" == "-silent" goto tail
	echo The '-silent' option is not supported.
	goto end
:tail
if not "%1" == "-tail" goto results
	echo The '-tail' option is not supported in this release.
	goto end
:results
if not "%1" == "-results" goto empty
	perl bin\wscollect %2 bin\runs
	goto end
:empty
:: # The -debug flag runs Webstone in debug mode and takes precedence 
:: # over the setting of DEBUG in the conf\testbed file.
if "%1" == "-debug" goto default
if "%1" == "" goto default
	echo  The '%1' option is not recognized.
	goto end
:default
	perl -Ibin bin\runbench.pl %1

:end
